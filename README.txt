Node registration dependent fields
==================================================

The Node registration dependent fields module makes Event field values/options depend on user's account fields values when the account
field on registration form will be changed.

The project consists of two modules:

Node registration dependent fields - serves as a core API and adds main functionality.
Node registration dependent fields (features) - adds current settings

Usage

1. Enable Node registration dependent fields module from the package.
2. Create a view with:
    - An Entity Reference display
    - Input/Contextual filter: Your argument-field type, e.g. if you pick from a list of nodes - the content id
    - Output: A list of items you would like to reference to e.g. taxonomy terms
3. Create field "field_event" on for node registration page for content type that you need
(admin/structure/node_registration)
    - on field settings for the entity selection choose Views: Filter with an entity reference view and choose the view
    what you need (event_codes_options _for_field - in default settings);
    - in field View arguments puts the User's account fields as args for the view.
    Some reserved tokens:
    {field_user_birthday} - returns the age for the date of birthday field of date type
    {current-node:nid} - returns nid of the current node
    {field_gen} - returns gender of the user