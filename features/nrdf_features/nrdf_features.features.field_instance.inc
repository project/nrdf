<?php
/**
 * @file
 * nrdf_features.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function nrdf_features_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node_registration-sports-field_event'.
  $field_instances['node_registration-sports-field_event'] = array(
    'bundle' => 'sports',
    'default_value' => NULL,
    'default_value_function' => 'entityreference_current_field_default_value',
    'default_value_widget' => array(
      'default_value_token' => '',
    ),
    'deleted' => 0,
    'description' => 'Event Code',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'parent' => '',
        'settings' => array(
          'bypass_access' => FALSE,
          'field_formatter_label' => '',
          'link' => FALSE,
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '',
            'linked' => 0,
          ),
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node_registration',
    'field_name' => 'field_event',
    'label' => 'Event Code',
    'options_limit' => FALSE,
    'options_limit_empty_behaviour' => 0,
    'options_limit_fields' => array(),
    'required' => 0,
    'settings' => array(
      'behaviors' => array(
        'current' => array(
          'action' => 'none',
          'action_on_edit' => 0,
          'fallback' => 'none',
          'skip_perm' => 0,
          'status' => 1,
          'use_uid' => 1,
        ),
        'prepopulate' => array(
          'status' => 0,
        ),
        'test_instance_behavior' => array(
          'status' => 0,
        ),
      ),
      'entity_translation_sync' => FALSE,
      'field_multiple_extended' => array(
        'enabled' => 0,
        'exclude_extra_item' => TRUE,
        'max_allowed' => '',
        'min_required' => '',
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'user-user-field_user_birthday'.
  $field_instances['user-user-field_user_birthday'] = array(
    'bundle' => 'user',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'field_formatter_label' => '',
          'format_type' => 'long',
          'fromto' => 'both',
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '',
            'linked' => 0,
          ),
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_user_birthday',
    'label' => 'Birthday',
    'required' => 0,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'entity_translation_sync' => FALSE,
      'field_multiple_extended' => array(
        'enabled' => 0,
        'exclude_extra_item' => 1,
        'max_allowed' => '',
        'min_required' => '',
      ),
      'user_register_form' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'parent' => '',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'd/m/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'multiple_value_widget' => 'table',
        'no_fieldset' => 0,
        'repeat_collapsed' => 0,
        'text_parts' => array(),
        'year_range' => '-150:+0',
      ),
      'type' => 'date_popup',
      'weight' => 10,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Birthday');
  t('Event Code');

  return $field_instances;
}
