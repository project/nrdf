<?php
/**
 * @file
 * nrdf_features.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function nrdf_features_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'event_codes_options_for_field';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Event codes options for field';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Event codes options for field';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event_codes' => 'event_codes',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['views_evi_plugins'] = FALSE;
  $handler->display->display_options['defaults']['views_evi_settings'] = FALSE;

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Gender (field_gender) */
  $handler->display->display_options['arguments']['field_gender_value']['id'] = 'field_gender_value';
  $handler->display->display_options['arguments']['field_gender_value']['table'] = 'field_data_field_gender';
  $handler->display->display_options['arguments']['field_gender_value']['field'] = 'field_gender_value';
  $handler->display->display_options['arguments']['field_gender_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_gender_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_gender_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_gender_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_gender_value']['limit'] = '0';
  /* Contextual filter: Content: Age Range range argument */
  $handler->display->display_options['arguments']['field_age_range']['id'] = 'field_age_range';
  $handler->display->display_options['arguments']['field_age_range']['table'] = 'field_data_field_age_range';
  $handler->display->display_options['arguments']['field_age_range']['field'] = 'field_age_range';
  $handler->display->display_options['arguments']['field_age_range']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_age_range']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_age_range']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_age_range']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['views_evi_plugins'] = FALSE;
  $handler->display->display_options['defaults']['views_evi_settings'] = FALSE;
  $export['event_codes_options_for_field'] = $view;

  return $export;
}
