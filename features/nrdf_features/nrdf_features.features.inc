<?php
/**
 * @file
 * nrdf_features.features.inc
 */

/**
 * Implements hook_views_api().
 */
function nrdf_features_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
